<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>A Detailed Look at Inceptions</title>
    <meta name="description" content="We power businesses with technology by linking IT and line-of-business goals. Today’s competitive business + environment demands technologies that collaborate effectively, innovate new market advantage, increase business performance + and enhance market share. We help your company grow, stay competitive and succeed.
">

    <link rel="stylesheet" href="http://sprway.bitbucket.org/css/main.css">
    <link rel="canonical" href="http://sprway.bitbucket.org/inceptions_detail.html">
</head>


  <body>

    <header class="site-header">

  <div class="wrapper">

    <a class="site-title" href="http://sprway.bitbucket.org/">The SPR Way for Agile Teams</a>

    <nav class="site-nav">
      <a href="#" class="menu-icon">
        <svg viewBox="0 0 18 15">
          <path fill="#424242" d="M18,1.484c0,0.82-0.665,1.484-1.484,1.484H1.484C0.665,2.969,0,2.304,0,1.484l0,0C0,0.665,0.665,0,1.484,0 h15.031C17.335,0,18,0.665,18,1.484L18,1.484z"/>
          <path fill="#424242" d="M18,7.516C18,8.335,17.335,9,16.516,9H1.484C0.665,9,0,8.335,0,7.516l0,0c0-0.82,0.665-1.484,1.484-1.484 h15.031C17.335,6.031,18,6.696,18,7.516L18,7.516z"/>
          <path fill="#424242" d="M18,13.516C18,14.335,17.335,15,16.516,15H1.484C0.665,15,0,14.335,0,13.516l0,0 c0-0.82,0.665-1.484,1.484-1.484h15.031C17.335,12.031,18,12.696,18,13.516L18,13.516z"/>
        </svg>
      </a>

    </nav>

  </div>

</header>


    <div class="page-content">
      <div class="wrapper">
        <div class="post">

  <header class="post-header">
    <h1 class="post-title">A Detailed Look at Inceptions</h1>
  </header>

  <article class="post-content">
    <p>The Inception is a critical, formative activity in our agile project lifecycle - it sets the tone for the entire project that follows. It represents, first and foremost, our opportunity to achieve a <em>clear, shared vision</em> - not only as the developers-to-be of the software, but among various project stakeholders as well (we never assume that all stakeholders agree on project scope or functionality coming into an Inception).</p>

<h2 id="goals">Goals</h2>

<p>The major goals of an Inception are:</p>

<ul>
  <li>Establish a shared vision of the product to be developed</li>
  <li>Ensure that all items in project scope have tangible busines value, and that those with the highest value are prioritized appropriately</li>
  <li>Define an acceptable technical solution (high level design and architecture)</li>
  <li>Create preliminary, high level development estimates and delivery plan</li>
  <li>Set stakeholder expectations regarding estimated project size (cost, duration, etc.)</li>
  <li>Identify and document key risks to success</li>
</ul>

<h2 id="deliverables">Deliverables</h2>

<p>Key deliverables include:</p>

<ul>
  <li>
    <p><strong>Story Map</strong> - an arrangement of stories in two-dimensional space that clusters stories around themes - making it easier to understand the entire application than viewing a long list of stories. This map forms naturally during workshops, by adhering story cards to walls in the room. On smaller projects, we may simply take photos of these maps and preserve those in a project Wiki or Basecamp site. On larger efforts with many stakeholders (especially those who didn’t participate in the Inception), there may be value in reproducing the story map in an electronic tool for easier accessibility to a wider audience.</p>
  </li>
  <li>
    <p><strong>Product Backlog</strong> - the same stories from the Story Map, loaded into a suitable tool (e.g. PivotalTracker, Jira Agile, etc.). Before the completion of the Inception, we will estimate all stories (using <em>story points</em>), as best we can given our imperfect/incomplete knowledge, so that we can provide at least a high level estimate of effort for the project.</p>
  </li>
  <li>
    <p><strong>Product Roadmap</strong> - A release plan that groups sets of related stories into releases - points at which the software produced can be meaningfully deployed to production in order to begin receiving business benefit. The roadmap will define the order in which stories are undertaken (based on priorities), and will define the key release points.</p>
  </li>
  <li>
    <p><strong>Architecture.</strong>  We will learn enough about non-functional requirements and architectural constraints to document at least the foundational architecture - things like platform and language (e.g. “Java with Spring and Hibernate, running on Linux on Amazon AWS”), as well as the key architectural layers within the application (simple Model/View/Controller? Services? Facades? Data Access?) We will document as much of this architecture as we can, given the knowledge we have built up through the Inception workshops.</p>
  </li>
  <li><strong>Design</strong>. While we don’t espouse “big up-front design,” neither to we want to enter development having done no design at all. As part of the high-level requirements capture process, we often produce several design deliverables:
    <ul>
      <li><strong>Wireframes</strong> - low-fidelity sketches of key parts of the user interface (where applicable)</li>
      <li><strong>Entiry/Relationship Diagrams</strong> - A relatively high level sketch of the database, with major domain entities included, as well as relationships between entities. Only significant attributes will be included (it’s not intended to be an all-inclusive, physical database design).</li>
      <li><strong>Domain Class Model</strong> - Similar in many ways to the E/R diagram, the domain class model is an object-oriented view of the project domain. This diagram is a bit more expressive, in that it can show OO relationships that cannot be expressed in a pure database diagram (e.g. inheritance). Note that this is a <em>domain</em> class diagram; it won’t include things like implementation classes, framework classes, etc. Its purpose is to help the team quickly understand the domain - its key entities and relationships among them.</li>
      <li><strong>High Level Architecture Diagram</strong> - A “box diagram” showing major application components and/or layers, and how they all communicate with one another. Includes things like databases and application servers, as well as key software elements (web servers, REST services, service buses / message queues, etc.).</li>
    </ul>
  </li>
  <li><strong>Risk Profile.</strong>  During inception workshops, we typically uncover a small number of project characteristics that give us pause. Maybe it’s a dependency on a bleeding-edge new open source framework. Or a thorny business problem that calls for a highly creative solution. Whatever they are, we usually recognize risks when we see them. And when we do see them during the course of an inception, we document them, think about them, and look for ways to minimize the risk. We will often include risk-mitigation activities in our Sprint Zero - things like architectural spikes or proof-of-concept work that prove early on in the project that what we <em>think</em> will work actually <strong>does</strong> work. The risk profile deliverable is simply a list of identified risks that we can begin to think about in preparation for Sprint Zero.</li>
</ul>

<h2 id="duration">Duration</h2>

<p>There is no “magic formula” we use to calculate the duration of an inception. Only rarely will an inception take less time than a week, and on many of our larger projects, inceptions can run from four to six weeks. Compressing an Inception into too small a time box adds risk to the project, as any estimates we generate coming out of the inceptions will be based on too little information, and therefore be so inaccurate as to be misleading.</p>

<p>As a very basic rule of thumb, one inception-week per anticipated 1,000 team hours is a reasonable guideline. For example, a project that “feels like” it could be done by two people in six months (2,000 hours) probably warrants an inception in the neighborhood of two weeks. Of course, this method involves estimating the overall project size <strong>before</strong> conducting the inception - when very little is understood about the effort - so we rely on past experience and common sense when sizing inception engagements.</p>

<h2 id="workshops--activities">Workshops &amp; Activities</h2>
<p>The Inception consists of a series of time-boxed workshops. The environment is highly visual and collaborative with collective output that is built from tangible models.</p>

<p>The workshop focus is on activities that drive the most value to the business. The process is also iterative and feedback driven; Each workshops builds upon the success of the prior workshop.</p>

<p>The table below lists the various workshops (meetings) that comprise an inception, and the activities and deliverables associated with each workshop. Not all workshops will have relevance to all projects, but in a majority of cases, all workshops will add value. We will plan on an inception having all workshops listed below unless we have discussed its relevance with our client ahead of time, and mutually agreed to omit it.</p>

<h3 id="kickoff">Kickoff</h3>

<p>This is a brief introductory session in which we will provide a high level orientation - an overview of the Inception process as a whole, and of the various workshops that will be executed along the way. This gives all participants and stakeholders a degree of comfort with the process, and will help participants understand where we are in that process as the workshops take place in the following days / weeks to come.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Describe plan going forward</li>
				<li>Clarify Participant Roles</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Business Representatives</li>
				<li>Technology Representatitves</li>
				<li>Project Stakeholders</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Shared understanding of Inception process, goals, and the manner in which various artifacts and discoveries build</li>
				<li>Set expectations</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>
			<ul>
				<li>Pre-planning meeting</li>
			</ul>
		</td>
	</tr>
</table>
<p><br /></p>

<h3 id="introduction">Introduction</h3>

<p>In this introductory workshop, we first begin to explore the subject matter - what we often call the ‘business domain’ - of the application or system to be built. We discuss, at a relatively high level, the intended goals of the new software, the benefits that will be expected to be received once it is complete, and the way success of the project will be measured. We will also identify and give names to <em>key business processes</em> - some set of specific processes within our client’s business through which they produce value for their customers. (At this point, they will only be named, and understood at a high level; we will add detail in a subsequent workshop.)</p>

<p>From our standpoint, as relative outsiders, this session is where we begin to learn about our client’s business - its processes, its terminology, current challenges, its opportunities and competitive threats - all the things we will need to know in order to work effectively with the client team in all of the workshops that follow, and hopefully, to be able to provide insight, from an outsider’s perspective, as the new system begins to take shape in the minds of the team.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Confirm understanding</li>
				<ul>
					<li>Business goals</li>
					<li>Expected benefits</li>
					<li>Success criteria</li>
				</ul>
				<li>Describe key risks/issues</li>
				<li>Describe assumptions and dependencies</li>
				<li>Define constraints (cost-scope-time-technical)</li>
				<li>Enumerate high level business processes and key actors of problem domain</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Stakeholders</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Business case</li>
				<li>Scope</li>
				<li>Operating constraints</li>
				<li>Non-functional requirements</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Kickoff</td>
	</tr>
</table>
<p><br /></p>

<h3 id="examination-current-state">Examination (Current State)</h3>

<p>In this workshop, we begin to examine the business domain more deeply by reviewing each of the current business processes identified in Introduction and describe them more fully on flipchart paper using activity and/or sequence diagrams drawn in colored markers. The materials are affixed to a large wall in the meeting room so the models can be continually reviewed and referred to as we go forward in subsequent workshops.</p>

<p>Through the discussions in this workshop, we will learn about our client’s domain - names for the key “things” that comprise the business in which the to-be-built application will live.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>(optional) Define project activity timeline for historical context</li>
				<li>Create high level data flow diagram for each process in the problem domain</li>
				<li>(optional) Annotate diagram with current pain points</li>
				<li>(optional) Document the domain vocabulary</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Business Representatives</li>
				<li>(optional) Entire team for activity timeline and pain points</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>(optional) Activity timeline</li>	
				<li>(optional) Historical context &amp; motivations</li>	
				<li>High level domain description</li>	
				<li>(optional) Pain points</li>	
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Introduction</td>
	</tr>
</table>
<p><br /></p>

<h3 id="envisioning-future-state">Envisioning (Future State)</h3>

<p>In this workshop, we take a deeper dive into each of the business processes identified in the preceding workshop and envision a process for the future. We will review the fundamental steps that occur in each existing business flow, and identify the actors who initiate each step.</p>

<p>At this point we believe it is important to step back, examine each of the business processes, and look for opportunities to make things better. Perhaps there are opportunities to rework business flows to make them more important. To shift responsibilities of actors (personas) to reduce the number of handoffs in a process. To introduce fresh, modern thinking to what may be years-old, well-established ways of doing business.</p>

<p>We like to spend some time in a “there are no bad ideas” frame of mind, tossing around ideas - no matter how audacious - for streamlining or otherwise improving business operations, before heading into the downstream workshops where we’ll begin to imagine how a new application might look, and hang together.</p>

<p>We will use flip chart paper and colored markers to dynamically build flow diagrams in collaboration with the team. The materials are affixed to a large wall in the meeting room so the models can be continually reviewed and improved as we go forward in subsequent workshops.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review business flows with identified pain points</li>
				<li>Discuss alternative approaches to those business flows</li>
				<li>Discuss real-world examples of alternate approaches that have been successful</li>
				<li>Create future-state process flow models (with actors and callouts to key systems)</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Business Representatives</li>
				<li>(optional) Entire team for pain points</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>List of key opportunities for improvement</li>
				<li>List of follow-up action items (e.g. seek approval for any 'radical' ideas)</li>				
				<li>Business activities for story mapping</li>
				<li>Epics (can be used in lieu of stories for product backlog)</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Examination</td>
	</tr>
</table>
<p><br /></p>

<h3 id="confirmation-user-personas">Confirmation (User Personas)</h3>

<p>In this session, we pause to consider the business domain from a different perspective - that of the groups of individuals who carry out the day-to-day activities we’ve just discussed in our going-forward business flows. We stop to discuss and consider each <em>persona</em> - what they do, what’s important to them, and what motivates them. We will cross-check our list of personas against our business flows, ensuring that every step in a flow has an associated persona (the “doer”), and that all personas we’ve listed have some function in our business flows. Through this discussion, we often discover missing steps in our flows, or personas we hadn’t thought of before. In that regard, this workshop is a great way for the team to check its work and ensure that our model of the business is as complete as possible.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review/refine list of users in the problem domain</li>
				<li>Document each user’s goals and motivations</li>
				<li>Document activities each user performs</li>
				<li>(optional) Annotate personas with current pain points</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Business Representatives</li>
				<li>(optional) Entire team for pain points</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>User roles</li>
				<li>User goals</li>
				<li>User activities</li>
				<li>Cross reference of activities and goals for story mapping</li>
				<li>(optional) Pain points</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Envisioning</td>
	</tr>
</table>
<p><br /></p>

<h3 id="system-flows">System Flows</h3>

<p>This workshop marks our first voyage into the world of technology. Up to this point, all discussions have been at a business level - focusing on <em>what</em> happens and <em>who</em> is involved. Now, we start to look at <em>how</em> things happen - or will happen, once the new system has been implemented. We will begin to explore how critical business information is captured, how it is transformed, and how it is ultimately used. We’ll talk about databases, UI screens, service API’s, and reports. Obviously, the participants in this session will include key technical stakeholders from our client.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review/refine technical constraints</li>
				<li>Document desired end state and identify issues/risks</li>
				<li>Document integration interfaces</li>
				<li>Document performance and security requirements</li>
				<li>Document error detection, recovery and reporting requirements for operations</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Technology Representatives</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Architecture model</li>
				<li>SLAs for performance and security</li>
				<li>Requirements for operations</li>
				<li>Additional non-functional requirements</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Envisioning, Confirmation</td>
	</tr>
</table>
<p><br /></p>

<h3 id="user-story-mapping">User Story Mapping</h3>

<p>This workshop is where the team really gets to work in terms of envisioning what the new system will look like, and how users will interact with it. Through the development of <em><a href="user_stories.html">user stories</a></em>, based on the business and system flows generated previously, the team will describe, at a relatively high level, the entire feature set of the system (including not just user-facing features, but also batch or automated features).</p>

<p>The process of <em>story mapping</em> is fully documented <a href="story_mapping.html">elsewhere</a>; briefly, it involves arranging user stories on index cards in such a way that stories are well-organized visually - grouped by theme, and often sorted by priority as well, so participants in the room can easily orient themselves and focus on a particular area of the business / application in conversation.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review/refine process flow models</li>
				<li>(optional) Review/refine system flow models</li>
				<li>Document key user activities</li>
				<li>Break down activities into user interactions (steps)</li>
				<li>(optional) Identify additional system interactions</li>
				<li>(optional) Identify additional administrative interactions</li>
				<li>(optional) Augment with conceptual UI paper prototypes</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Business Representatives</li>
				<li>Technology Representatives optional, for system interactions)</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Product backlog</li>
				<li>(optional) Tech tasks</li>
				<li>(optional) User experience metaphor</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Envisioning</td>
	</tr>
</table>
<p><br /></p>

<h3 id="prioritization">Prioritization</h3>

<p>In User Story Mapping workshop(s) that precede this one, we will have built an understanding of system functionality and documented it in the form of a plethora of user stories. And while in the <em>mapping</em> process we may have touched on relative priorities of stories a bit, we won’t have considered priority as a primary focus of discussion.</p>

<p>In this workshop, we do just that. In order not to get bogged down in discussions over too-fine relative measures of priority, we generally use a three-priority system: must have, should have, and could have. Every story card will be assigned a priority. Often times, we’ll start to rearrange stories, with higher-priority items nearer the top and lower-priority items nearer the bottom (to aid in at-a-glance comprehension).</p>

<p>The priorities assigned in this workshop are a “going-in position” and will be important as input into release planning. However, we will constantly review priorities (and, inevitably, the content of the stories themselves) throughout the forthcoming development effort to reflect changes that occur over time.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review user stories, system stories and tasks</li>
				<li>Assign priorities</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Priorities for release plan</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Complete product backlog (stories or epics)</td>
	</tr>
</table>
<p><br /></p>

<h3 id="estimation">Estimation</h3>

<p>In this workshop, we revisit each story, with an eye toward determining its relative “size” (a subjective measure that takes into account the scope of work in a story as well as the relative complexity of that work). Stories are assigned points using a scale with a limited number of values (e.g. Fibonacci, using 1, 2, 3, 5 and 8). In order to solicit valuable perspectives from multiple participants, we use techniques like estimating poker, wherein all participants declare their “vote” on relative complexity at once, so as not to give anyone undue influence over others. In the end, of course, the team agrees on one estimate (story point value) for each story.</p>

<p>Once the stories have been sized, we will be in a position to provide a high level (budgetary) estimate for the entire development effort - and to begin roughing out a schedule that includes multiple releases, if appropriate.</p>

<p>For more on our estimating techniques <a href="estimating.html">click here</a>.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Review user stories, system stories and tasks</li>
				<li>Assign estimates</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Estimates for release plan</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Complete product backlog (stories or epics)</td>
	</tr>
</table>
<p><br /></p>

<h3 id="analysis--deliverable-generation">Analysis &amp; Deliverable Generation</h3>

<p>This “workshop” is a working session wherein the SPR Consulting inception team will step back, review all of the work accomplished in prior workshops, and begin to do final organizational and analytical work, with an eye toward producing the final set of deliverables for the inception.</p>

<p>In most inceptions, this work does not all take place in one long working session, after all preceding workshops are complete. Rather, we typically schedule inceptions as half-day (morning) working sessions with our clients, and reserve afternoons for internal discussion, analysis and consolidation. However, there is a certain amount of work that can only be done once we have a complete backlog of user stories that have been documented, prioritized and estimated. We use this working session to assimilate all of this freshly-created information and coalesce it into a final set of deliverables that not only capture our work, but which will be comprehensible to project stakeholders who may not have been direct participants in the inception process.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Organize stories into themes</li>
				<li>Package themes into releases</li>
				<li>Complete resource plan</li>
				<li>Complete fully loaded projections</li>
				<li>Compile results package into presentation</li>
				<li>Solicit feedback</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>SPR Consutling Team</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>Product road map</li>
				<li>Finalized versions of all above deliverables</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>All workshops above</td>
	</tr>
</table>
<p><br /></p>

<h3 id="final-readout">Final Readout</h3>

<p>This is a final meeting, including our team and our client’s core team representatives, in which we present the final set of deliverables we have just created. We will not dive into story-level details, but will describe a project roadmap - our story backlog laid out across time, showing approximately when key sets of deliverables (releases) will be complete.</p>

<p>We will also present a high level estimate of effort and cost for completing the project, based on the stories and estimates in the backlog. We will present what we believe to be an optimal team for the development effort, including disciplines like project management (ScrumMaster), business analysis and quality assurance, as well as the more obvious software engineering.</p>

<table style="width: 740px; font-size: 0.8em;">
	<tr>
		<td><strong>Activities</strong></td>
		<td>
			<ul>
				<li>Deliver Presentation</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Attendees</strong></td>
		<td>
			<ul>
				<li>Core Team</li>
				<li>Stakeholders</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Purpose / Outcomes</strong></td>
		<td>
			<ul>
				<li>
					Decision to proceed with development
				</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><strong>Prerequisites</strong></td>
		<td>Analysis &amp; Deliverable Generation</td>
	</tr>
</table>
<p><br /></p>


  </article>

</div>

      </div>
    </div>

    <footer class="site-footer">

  <div class="wrapper">

    <h2 class="footer-heading">The SPR Way for Agile Teams</h2>

    <div class="footer-col-wrapper">
      <div class="footer-col  footer-col-1">
        <ul class="contact-list">
          <li>The SPR Way for Agile Teams</li>
          <li><a href="mailto:info@spr.com">info@spr.com</a></li>
        </ul>
      </div>

      <div class="footer-col  footer-col-2">
        <ul class="social-media-list">
          
        </ul>
      </div>

      <div class="footer-col  footer-col-3">
        <p class="text">We power businesses with technology by linking IT and line-of-business goals. Today’s competitive business + environment demands technologies that collaborate effectively, innovate new market advantage, increase business performance + and enhance market share. We help your company grow, stay competitive and succeed.
</p>
      </div>
    </div>

  </div>

</footer>


  </body>

</html>
